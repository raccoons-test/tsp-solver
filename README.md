TSP SOLVER
===

This is test application used to solve traveling salesman problem.

Usage
---

1. First install dependencies via `npm install`
2. Run `npm start` to show additional information.
