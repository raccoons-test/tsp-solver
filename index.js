const fs = require('fs')
const yargs = require('yargs')

const argv = yargs
    .option('input', {
        alias: 'i',
        describe: 'Specifies input file path.',
        type: 'string',
        requiresArg: true
    })
    .option('output', {
        alias: 'o',
        describe: 'Specifies output file path.',
        type: 'string',
        default: 'output.csv'
    })
    .help('help')
    .alias('h', 'help')
    .argv

/**
 * Read file with points.
 */

if (!argv.input) {
    console.error('No input file specified.')
    process.exit(0)
}

if (!argv.output) {
    console.error('No output file specified.')
    process.exit(0)
}

let points = fs
    .readFileSync(argv.input, { encoding: 'utf8' })
    .split(/\r?\n/)
    .map(line => {
        line = line.trim()
        if (!line) return null
        let parts = line.split(',')
        return parts.length == 2 ? { x: +parts[0], y: +parts[1] } : null
    })
    .filter(v => !!v)

/**
 * Calculate path length.
 * 
 * @param {Array} points 
 */
function length(points) {
    let len = 0
    if (!points || points.length < 2) {
        return len
    }

    for (let i = 1; i < points.length; i++) {
        let prev = points[i - 1]
        let current = points[i]
        let diff = {
            x: current.x - prev.x,
            y: current.y - prev.y
        }
        len += diff.x * diff.x + diff.y * diff.y
    }

    return Math.sqrt(len)
}

/**
 * Swaps two array items.
 * 
 * @param {Array} array 
 * @param {Number} i 
 * @param {Number} j 
 */
function swap(array, i, j) {
    [array[i], array[j]] = [array[j], array[i]]
}

/**
 * Performs number of permutations.
 * 
 * @param {Array} array 
 * @param {Number} cycles 
 */
function permute(array, cycles) {
    for (let i = 0; i < cycles; i++)
        swap(array, rand(array.length), rand(array.length))
}

/**
 * Generates random number from 0 to len.
 * 
 * @param {Number} len 
 */
function rand(len) {
    return Math.floor(Math.random() * len)
}

/**
 * Moves all values from one arr to another.
 * 
 * @param {Array} from
 * @param {Array} to
 */
function move(from, to) {
    from.forEach((v, i) => to[i] = v)
}

/**
 * Returns comma separated representation for
 * the given array of points.
 * 
 * @param {Array} points 
 */
function formatPointsCsv(points) {
    return points.map(p => `${p.x},${p.y}`).join('\n')
}

/**
 * Performs 2-opt and swap algorithm.
 * 
 * @param {Array} points 
 */
function twoOpt(points) {
    let len = length(points)
    for (let i = 0; i < points.length; i++) {
        for (let j = 0; j < points.length; j++) {
            if (i === j) continue
            swap(points, i, j)
            let newLen = length(points)
            if (newLen < len) {
                len = newLen
                i = 0
                break
            } else {
                swap(points, i, j)
            }
        }
    }

    return length(points)
}

/**
 * Start TSP solving.
 * 
 * In order to overcome stuck in local minima,
 * I just make small permutations on the input.
 * I know that this is not the fastest and optimal solution
 * as it uses similar approach as in genetic algorithms, but
 * I didn't care :)
 */

let len = length(points), _len = len
let _points = points.slice()

console.log('Initial path length: ', len)

for (let i = 0; i < 100; i++) {
    len = twoOpt(points)
    permute(_points, 2)
    _len = twoOpt(_points)
    if (_len < len) {
        move(_points, points)
    } else {
        move(points, _points)
    }
}

console.log('Optimal path length: ', length(points))
fs.writeFileSync(argv.output, formatPointsCsv(points))
